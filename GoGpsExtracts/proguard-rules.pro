# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#指定代码的压缩级别
-optimizationpasses 5
#包明不混合大小写
-dontusemixedcaseclassnames
#不去忽略非公共的库类
-dontskipnonpubliclibraryclasses
#混淆时是否记录日志
-verbose
#优化  不优化输入的类文件
-dontoptimize
#预校验
-dontpreverify
# 保留sdk系统自带的一些内容 【例如：-keepattributes *Annotation* 会保留Activity的被@override注释的onCreate、onDestroy方法等】
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
# 避免混淆泛型
-keepattributes Signature
# 抛出异常时保留代码行号,保持源文件以及行号
-keepattributes SourceFile,LineNumberTable
-ignorewarnings

-keep public class * extends android.app.Activity                               # 保持哪些类不被混淆
-keep public class * extends android.app.Application                            # 保持哪些类不被混淆
-keep public class * extends android.app.Service                                # 保持哪些类不被混淆
-keep public class * extends android.content.BroadcastReceiver                  # 保持哪些类不被混淆
-keep public class * extends android.content.ContentProvider                    # 保持哪些类不被混淆
-keep public class * extends android.app.backup.BackupAgentHelper               # 保持哪些类不被混淆
-keep public class * extends android.preference.Preference                      # 保持哪些类不被混淆
-keep public class com.android.vending.licensing.ILicensingService              # 保持哪些类不被混淆

-keepclasseswithmembernames class * {                                           # 保持 native 方法不被混淆
    native <methods>;
}

# 保持自己定义的类不被混淆
-keep class com.galfins.gogpsextracts.Constellations.Constellation {
    public *;
}

-keep class com.galfins.gogpsextracts.Corrections.Correction {
    public *;
}

-keep class com.galfins.gogpsextracts.PvtMethods.PvtMethod {
    public *;
}

-keep class com.galfins.gogpsextracts.Constellations.BdsConstellation {
    private *;
}

-keep class com.galfins.gogpsextracts.Constellations.BdsGpsConstellation {
    public *;
}

-keep class com.galfins.gogpsextracts.Constellations.GalileoConstellation {
    public *;
}

-keep class com.galfins.gogpsextracts.Constellations.GpsConstellation {
    public *;
}

-keep class com.galfins.gogpsextracts.Constellations.SatelliteParameters {
    public *;
}

-keep class com.galfins.gogpsextracts.Coordinates {
    public *;
}

-keep class com.galfins.gogpsextracts.Corrections.ShapiroCorrection {
    public *;
}

-keep class com.galfins.gogpsextracts.Corrections.TropoCorrection {
    public *;
}

-keep class com.galfins.gogpsextracts.Corrections.IonoCorrection {
     public *;
}

-keep class com.galfins.gogpsextracts.PvtMethods.WeightedLeastSquares {
    public *;
}
